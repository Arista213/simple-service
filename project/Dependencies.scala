import sbt._

object Dependencies {
  val catsCore = "2.7.0"
  val catsEffect = "3.3.7"
  val AkkaVersion = "2.6.8"
  val AkkaHttpVersion = "10.2.9"
  val dockerVersion = "8.16.0"


  val dependencies =
    Seq(
      "org.typelevel" %% "cats-core" % catsCore,
      "org.typelevel" %% "cats-effect" % catsEffect,
      "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
      "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
      "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
      "com.spotify" % "docker-client" % dockerVersion
    )
}