import com.typesafe.sbt.SbtNativePackager.{Docker, Universal}
import com.typesafe.sbt.packager.Keys.{daemonUser, dockerBaseImage, dockerExposedPorts, packageName}
import com.typesafe.sbt.packager.MappingsHelper.contentOf
import sbt.Keys._
import sbt._

object Settings {

  val commonSettings =
    Seq(
      scalaVersion := "2.13.8",
      packageName in Docker := "service",
      version := (version in ThisBuild).value,
      version in Docker := version.value,
      dockerBaseImage := "openjdk:11-jre",
      dockerExposedPorts := Seq(8080),
      daemonUser in Docker := "root",
      mappings in Universal ++= contentOf("items")
    )
}