import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import java.io.{File, IOException}
import java.nio.file.{Files, Paths}
import scala.concurrent.{ExecutionContextExecutor, Future}

object Service {
  implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "my-system")
  implicit val executionContext: ExecutionContextExecutor = system.executionContext

  def main(args: Array[String]): Unit = {
    val route: Route =
      path(Segments) { path =>
        get {
          val result = isFileExist(path.mkString("/"))
          onSuccess(result) {
            case Some(result) => complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, result))
            case None => complete(StatusCodes.NotFound)
          }
        }
      }

    Http().newServerAt("0.0.0.0", 8080).bind(route)
  }

  def isFileExist(path: String): Future[Option[Array[Byte]]] = Future {
    try {
      val bytes = Files.readAllBytes(Paths.get(path))
      Option(bytes)
    }
    catch {
      case _: IOException => None
    }
  }
}